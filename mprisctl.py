#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

u"""
mprisctl
--------

Control any MPRIS-compatible player via DBus.

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""

from __future__ import print_function

import sys
from optparse import OptionParser

import dbus


PLAYERS = ("clementine", "amarok", "vlc", "spotify")


class BaseController(object):

    base_service = path_name = base_method = None

    def __init__(self, name, bus=None, debug=False):
        self.name = name
        self.bus = bus
        self.debug = debug
        self.iface = None

    def connect(self):
        if self.bus is None:
            self.bus = dbus.SessionBus()
        proxy = self.bus.get_object('%s.%s' % (self.base_service, self.name),
                                    self.path_name)
        self.iface = dbus.Interface(proxy, dbus_interface=self.base_method)

    def _call(self, cmdname, *args):
        if self.debug:
            print("D: Calling command %s on %s" % (cmdname, self.name))
        method = getattr(self.iface, cmdname)
        result = method(*args)
        if self.debug:
            print(" --> ", result)
        return result

    def cmd_playpause(self):
        self._call("PlayPause")
    def cmd_pause(self):
        self._call("Pause")
    def cmd_stop(self):
        self._call("Stop")
    def cmd_previous(self):
        self._call("Previous")
    def cmd_next(self):
        self._call("Next")
    def cmd_forward(self):
        raise NotImplementedError
    def cmd_rewind(self):
        raise NotImplementedError


class MPrisController(BaseController):
    """
    Use the MPRIS v1 interface
    """

    base_service = "org.mpris"
    path_name = "/Player"
    base_method = "org.freedesktop.MediaPlayer"

    def cmd_forward(self):
        pos = self._call("PositionGet")
        self._call("PositionSet", pos + 10*1000)
    def cmd_rewind(self):
        pos = self._call("PositionGet")
        self._call("PositionSet", pos - 10*1000)


class MPris2Controller(BaseController):
    """
    Use the MPRIS v2 interface
    """

    base_service = "org.mpris.MediaPlayer2"
    path_name = "/org/mpris/MediaPlayer2"
    base_method = "org.mpris.MediaPlayer2.Player"

    def cmd_forward(self):
        self._call("Seek", 10 * 1000000)
    def cmd_rewind(self):
        self._call("Seek", -10 * 1000000)


#MPris1Players = ("clementine")
MPris1Players = ()
def get_controller(name, session_bus, debug):
    if name in MPris1Players:
        return MPrisController(name, session_bus, debug=debug)
    else:
        return MPris2Controller(name, session_bus, debug=debug)


def main():

    available_commands = [ c[4:] for c in dir(BaseController)
                           if c.startswith("cmd_") ]

    description = ("Available commands: %s.\nAvailable players: %s.\n"
                   "If no player name is specified on the command line, "
                   "all known players are sent the command."
                    % (", ".join(available_commands), ", ".join(PLAYERS)))
    parser = OptionParser(usage="%prog [-n player] command",
                          description="Available commands: %s"
                            % ", ".join(available_commands),
                          )
    parser.add_option("-n", "--name", help="player name")
    parser.add_option("-d", "--debug", action="store_true", help="be verbose")
    opts, args = parser.parse_args()

    if len(args) != 1:
        parser.error("wrong number of arguments")

    name = opts.name
    command = args[0]

    if command not in available_commands:
        parser.error("unknown command: %s. Available commands: %s."
                     % (command, ", ".join(available_commands)))

    if opts.name:
        players = (opts.name, )
    else:
        players = PLAYERS
    session_bus = dbus.SessionBus()
    for name in players:
        if opts.debug:
            print("D: Trying player %s..." % name)
        controller = get_controller(name, session_bus, debug=opts.debug)
        try:
            controller.connect()
        except dbus.exceptions.DBusException:
            if opts.name:
                print("Can't connect to %s, aborting." % name, file=sys.stderr)
                sys.exit(1)
            if opts.debug:
                print("D: Could not connect to player %s" % name)
            continue
        method = getattr(controller, "cmd_" + command)
        method()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\rExiting on user request")
