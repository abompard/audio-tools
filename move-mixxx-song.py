#!/usr/bin/env python3

import os
import sqlite3
import sys

from argparse import ArgumentParser


def parse_args(argv):
    parser = ArgumentParser()
    parser.add_argument("-f", "--force", action="store_true", default=False)
    parser.add_argument("source", nargs="+")
    parser.add_argument("destination")
    args = parser.parse_args(argv)
    sources, destination = args.source, args.destination
    if not destination:
        parser.error("destination required")
    if os.path.exists(destination):
        if not os.path.isdir(destination) and not args.force:
            parser.error("file already exists: %s" % destination)
    elif not os.path.exists(os.path.dirname(destination)):
        parser.error("no such directory: %s" % os.path.dirname(destination))
    return sources, destination

def move_in_db(source, destination, dbconn):
    cursor = dbconn.cursor()
    cursor.execute("SELECT COUNT(*) FROM track_locations where location = ?", (source,))
    found = bool(cursor.fetchone()[0])
    if not found:
        return # Not in the DB? Perfect, just move it on the filesystem.
    cursor.execute("UPDATE track_locations SET location = ?, directory = ? "
                   "WHERE location = ?",
                   (destination, os.path.dirname(destination), source))
    if cursor.rowcount == 1:
        dbconn.commit()
    else:
        print("Failed to update the database. Affected rows: %s"
              % dbconn.total_changes, file=sys.stderr)
        dbconn.rollback()
    cursor.close()

def move_in_fs(source, destination):
    os.rename(source, destination)


def main(argv):
    sources, destination = parse_args(argv)
    dbconn = sqlite3.connect(os.path.expanduser("~/.mixxx/mixxxdb.sqlite"))
    for source in sources:
        if not os.path.exists(source):
            print("No such file: %s" % source, file=sys.stderr)
            continue
        source = os.path.abspath(source)
        destination = os.path.abspath(destination)
        if os.path.isdir(destination):
            real_dest = os.path.join(destination, os.path.basename(source))
        else:
            real_dest = destination
        print("%s -> %s" % (source, real_dest))
        move_in_fs(source, real_dest)
        move_in_db(source, real_dest, dbconn)
    dbconn.close()


if __name__ == "__main__":
    main(sys.argv[1:])
