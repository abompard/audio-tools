#!/bin/sh

playlist=$1

if [ ! -e "$playlist" ]; then
    echo "File not found: $playlist"
    exit 1
fi

(
    echo 0
    grep -v -e '^$' -e '^#' "$playlist" | while read filename; do
        midentify "$(dirname "$playlist")/$filename" | grep '^ID_LENGTH=' | cut -d= -f2
        echo +
    done
    echo "sa la 60 / n [:] n la 60 % p"
) | dc
