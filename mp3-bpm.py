#!/usr/bin/env python3
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

"""
mp3-bpm
-------

Set the BPM in an MP3 file using http://echonest.com

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""

from __future__ import print_function, unicode_literals

import os
import argparse
import logging
import re
import sys
import subprocess as sp
from textwrap import dedent
try:
    from configparser import ConfigParser
    from urllib.parse import quote_plus
except ImportError:
    # Python 2. Not entirely compatible, so I'll remove that one day.
    from ConfigParser import ConfigParser
    from urllib import quote_plus

import requests
from mutagen.id3 import ID3, TBPM


LOG_CONFIG = {
    "level": logging.INFO,
    "format": '%(asctime)s %(message)s',
    "datefmt": '%H:%M:%S'
}
log = logging.getLogger(__name__)

ID3V2_OUTPUT_RE = re.compile(r'^([A-Z0-9]{4})\s\([^:]+\):\s(.*)$')


class NotFoundInEchonest(Exception):
    pass


def read_tags(filename):
    return ID3(filename)
    tags = EasyID3(filename)
    result = {}
    output = sp.check_output(["id3v2", "-l", filename])
    for line in output.decode("utf-8").split("\n"):
        match = ID3V2_OUTPUT_RE.match(line)
        if not match:
            continue
        result[match.group(1)] = match.group(2)
    return result


def find_bpm(artist, title, length, config):
    url = ("http://developer.echonest.com/api/v4/song/search?"
           "bucket=audio_summary&bucket=id:spotify&limit=true&"
           "api_key={}&artist={}&title={}".format(
            config["echonest_api_key"],
            quote_plus(artist, safe=""),
            quote_plus(title, safe="")))
    result = requests.get(url)
    try:
        songs = result.json()["response"]["songs"]
    except KeyError:
        raise NotFoundInEchonest
    if not songs:
        raise NotFoundInEchonest
    song = songs[0]
    if length is not None:
        result_length = song["audio_summary"]["duration"]
        if abs(int(length) - int(result_length)) > 2:
            log.warning(
                "Warning: different lengths for %s - %s "
                "(mp3 is %s, echonest is %s)",
                artist, title, length, result_length)
    bpm = songs[0]["audio_summary"]["tempo"]
    return bpm


def set_bpm(filename, config):
    tags = read_tags(filename)
    if "TBPM" in tags and not config["overwrite"]:
        log.debug("Existing BPM tag found: %s", tags.getall("TBPM"))
        return
    artist = str(tags.getall("TPE1")[0])
    title = str(tags.getall("TIT2")[0])
    length = int(str(tags.getall("TLEN")[0])) / 1000
    log.info("Processing %s - %s", artist, title)
    try:
        bpm = find_bpm(artist, title, length, config)
    except NotFoundInEchonest:
        log.warning("Song was not found in Echonest, skipping.")
        return
    log.debug("Found BPM: %s", bpm)
    tags.add(TBPM(encoding=3, text=str(bpm)))
    tags.save(v2_version=tags.version[1])


def read_config():
    configfile = ConfigParser()
    configfile.read([
        'mp3-bpm.cfg',
        os.path.expanduser('~/.config/mp3-bpm.cfg'),
        '/etc/mp3-bpm.cfg'])
    if not configfile.has_option("config", "echonest_api_key"):
        print(dedent("""\
        You must create a configuration file with your Echonest API key.
        The file path should be {} and the contents should be:

        [config]
        echonest_api_key = YOUR-API-KEY

        Go to http://developer.echonest.com/account/register to get an API key
        if you don't have one already.""").format(
            os.path.expanduser('~/.config/mp3-bpm.cfg')), file=sys.stderr)
        sys.exit(1)
    return dict(configfile.items("config"))


def parse_args():
    parser = argparse.ArgumentParser(
        description='Set the BPM in MP3 files.')
    parser.add_argument('filenames', nargs="+",
        help='MP3 files to set')
    parser.add_argument('--overwrite', action='store_true',
        help='overwrite existing BPM tag')
    parser.add_argument('--debug', action='store_true',
        help='output more information')
    args = parser.parse_args()
    for filename in args.filenames:
        if not os.path.exists(filename):
            parser.error("no such file: {}".format(filename))
    return args


def main():
    args = parse_args()
    if args.debug:
        LOG_CONFIG["level"] = logging.DEBUG
    logging.basicConfig(**LOG_CONFIG)
    requests_logger = logging.getLogger("requests")
    requests_logger.setLevel(logging.WARNING)
    config = read_config()
    config.update({
        "overwrite": args.overwrite,
    })
    for filename in args.filenames:
        set_bpm(filename, config)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.info("Exiting on user request...")
