#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

u"""
spotify-to-mp3
--------------

Dump audio output of Spotify to MP3 files.

Inspiration:
- https://gist.github.com/djmaze/5234008
- http://askubuntu.com/questions/60837/record-a-programs-output-with-pulseaudio

TODO:
- Use https://songbpm.com to store BPM in the TBPM ID3v2 tag (use the python
  binding or the id3v2 command)

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""

from __future__ import print_function, unicode_literals

import os
import atexit
import argparse
import logging
import re
import sys
import signal
import subprocess as sp
from tempfile import mkstemp

import glib
import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gobject import GObject

FILENAME_FORBIDDEN = r'"/\*?<>|:#'

LOG_CONFIG = {
    "level": logging.INFO,
    "format": '%(asctime)s %(message)s',
    "datefmt": '%H:%M:%S'
}
log = logging.getLogger(__name__)

MAINLOOP = None


class SpotifyNotFound(Exception):
    pass


class Encoder:
    """
    This class will serialize the encodings to avoid using too much CPU and
    disturbing WAV recording.
    """

    def __init__(self):
        self._queue = []
        self._working = False
        self._on_jobs_done = None
        self._timer_start_encode = None

    def do_when_finished(self, callback):
        """The callback will be called when all jobs are done"""
        assert callable(callback)
        self._on_jobs_done = callback
        if self.is_idle:
            callback()

    @property
    def is_idle(self):
        return not self._working and len(self._queue) == 0

    def encode(self, song_data):
        self._queue.append(song_data)
        self._timer_start_encode = glib.timeout_add_seconds(1, self._process_queue)

    def _process_queue(self):
        # We don't want this function to run periodically, just be delayed, so
        # remove the event source.
        if self._timer_start_encode is not None:
            glib.source_remove(self._timer_start_encode)
            self._timer_start_encode = None
        # Only one job at a time, this function will be called again when done.
        if self._working:
            return
        try:
            song_data = self._queue.pop(0)
        except IndexError:
            return # Nothing to encode
        log.debug("Started encoding %s - %s",
            song_data["track"]["artist"], song_data["track"]["title"])
        self._working = True
        enc_pid, stdin, stdout, stderr = glib.spawn_async(
            [b"nice", b"-n", b"19", b"lame",
             b"--quiet", b"-q", b"0", b"-b", b"320", b"--id3v2-only",
             b"--ta", song_data["track"]["artist"].encode("utf-8"),
             b"--tt", song_data["track"]["title"].encode("utf-8"),
             b"--tl", song_data["track"]["album"].encode("utf-8"),
             b"--tc", song_data["track"]["track_id"].encode("utf-8"),
             song_data["tmp_path"].encode("utf-8"),
             song_data["encoded_path"].encode("ascii"),
             ],
             flags=glib.SPAWN_SEARCH_PATH | glib.SPAWN_STDOUT_TO_DEV_NULL
                 | glib.SPAWN_DO_NOT_REAP_CHILD)
        glib.child_watch_add(enc_pid, self._on_enc_done, song_data)

    def _on_enc_done(self, pid, condition, user_data):
        """Called when encoding is done, stores the recorded file name."""
        log.debug("Done encoding %s - %s to %s",
            user_data["track"]["artist"], user_data["track"]["title"],
            os.path.basename(user_data["encoded_path"]))
        os.remove(user_data["tmp_path"])
        self._working = False
        # Encode the next one in the queue
        if len(self._queue):
            self._process_queue()
        elif self._on_jobs_done:
            self._on_jobs_done()


class Playlist:

    def __init__(self):
        self._recorded = []

    def already_recorded(self, track):
        track_ids = [ t["track_id"] for t in self._recorded ]
        return track["track_id"] in track_ids

    def add(self, song_data):
        song_data["track"]["filename"] = song_data["encoded_path"]
        self._recorded.append(song_data["track"])

    def write(self, path):
        """Write the M3U playlist with the recorded songs in order."""
        playlist_path = os.path.join(path, "playlist.m3u")
        with open(playlist_path, "w") as playlist:
            playlist.write(b"#EXTM3U\n\n")
            for track in self._recorded:
                playlist.write(b"#EXTINF:{},{} - {}\n{}\n\n".format(
                    track["length"],
                    track["artist"].encode("utf-8"),
                    track["title"].encode("utf-8"),
                    os.path.basename(track["filename"].encode("ascii"))))
        log.info("Wrote playlist to %s", playlist_path)



class Recorder:
    """
    Handles the recording of the stream and the encoding of the files on track
    change.
    """

    def __init__(self, path, playlist, encoder):
        self.path = path
        self.playlist = playlist
        self.encoder = encoder
        self._current_track = None
        self.rec_pid = None
        self.player = None
        self._tmpfile = None

    @property
    def is_playing(self):
        return self._current_track is not None

    def record(self, track):
        """Starts recording, called on track change."""
        if self._current_track == track:
            return # No change, just a useless DBus signal.
        if (self.is_playing and
                self._current_track["track_id"] == track["track_id"]):
            # Metadata update (track length update sometimes, looks like an
            # integer rounding thing)
            self._current_track = track
            return
        self.change_track(track)
        log.info("Now playing %s - %s",
            self._current_track["artist"], self._current_track["title"])
        if self.playlist.already_recorded(track):
            log.info("This song was seen before, maybe the playlist looped? Stopping now.")
            self.player.stop()
            self.finish()
            return
        # Check for an existing file
        if self._check_exists():
            log.info("Already encoded, skipping to the next song")
            self.player.skip_next()
            return
        # Record the current song as WAV in a temporary file
        _tmpfd, self._tmpfile = mkstemp(
            dir=self.path, prefix="spotify-dump-", suffix=".wav")
        os.close(_tmpfd)
        self.rec_pid, stdin, stdout, stderr = glib.spawn_async(
            [b"parecord", b"-d", b"spotdump.monitor",
             b"--volume=65536", b"--latency-msec=1",
             b"--file-format=wav", self._tmpfile.encode("utf-8")],
            flags=glib.SPAWN_SEARCH_PATH | glib.SPAWN_STDOUT_TO_DEV_NULL)
        log.debug("Recording started for %s - %s",
            self._current_track["artist"], self._current_track["title"])

    def _stop_recording_and_encode(self):
        """Stops recording and encodes the file to mp3."""
        # Stop the WAV recorder
        if self.rec_pid:
            # may be empty if recording never started (existing song)
            log.debug("Recording stopped for %s - %s",
                self._current_track["artist"], self._current_track["title"])
            try:
                os.kill(self.rec_pid, signal.SIGINT)
            except OSError as e:
                if e.errno != 3:
                    raise
        encoded_path = self._get_encoded_path()
        song_data = {
            "encoded_path": encoded_path,
            "track": self._current_track,
            "tmp_path": self._tmpfile,
        }
        if not self._check_exists():
            self.encoder.encode(song_data)
        self.playlist.add(song_data)

    def stop(self):
        """Stops recording, encodes the file to mp3, writes playlist and quits."""
        if not self.is_playing:
            return # We aren't recording anything, stop is a no-op.
        self._stop_recording_and_encode()
        self.finish()
        self._current_track = None

    def change_track(self, track):
        if self.is_playing:
            self._stop_recording_and_encode()
        self._current_track = track

    def finish(self):
        self.encoder.do_when_finished(self._finish)
        self.player.teardown()

    def _finish(self):
        self.playlist.write(self.path)
        MAINLOOP.quit()

    def _get_encoded_path(self):
        filename = "{} - {}.mp3".format(
            self._current_track["artist"], self._current_track["title"]
            ).encode("ascii", "replace").decode("ascii")
        for char in FILENAME_FORBIDDEN:
            filename = filename.replace(char, " ")
        return os.path.join(self.path, filename.strip())

    def _check_exists(self):
        filename = os.path.basename(self._get_encoded_path())
        for root, dirs, files in os.walk(self.path):
            if filename in files:
                return True
            # don't visit hidden directories
            dirs = [d for d in dirs if not d.startswith(".")]
        return False



class DBusManager:
    """Handles the connection to DBus."""

    def __init__(self, recorder):
        self.recorder = recorder
        self._session_bus = None
        self._proxy = None

    def setup(self):
        DBusGMainLoop(set_as_default=True)
        try:
            self._session_bus = dbus.SessionBus()
        except dbus.exceptions.DBusException:
            log.error("Could not connect to DBus")
            raise
        signal = self._session_bus.add_signal_receiver(
            self.on_signal, path='/org/mpris/MediaPlayer2',
            sender_keyword='sender')

    def teardown(self):
        self._session_bus.remove_signal_receiver(
            self.on_signal, path='/org/mpris/MediaPlayer2',
            sender_keyword='sender')

    def on_signal(self, method, data, signature, sender):
        # http://dbus.freedesktop.org/doc/dbus-python/doc/tutorial.html#receiving-signals
        if method != "org.mpris.MediaPlayer2.Player":
            return
        if "Metadata" not in data:
            return
        playback_status = data["PlaybackStatus"]
        if playback_status == "Paused":
            self.recorder.stop()
            return
        if playback_status == "Playing":
            new_track = {
                "artist": " & ".join([
                    unicode(a) for a in data["Metadata"]["xesam:artist"]]),
                "album": unicode(data["Metadata"]["xesam:album"]),
                "title": unicode(data["Metadata"]["xesam:title"]),
                "track_num": int(data["Metadata"]["xesam:trackNumber"]),
                "disc_num": int(data["Metadata"]["xesam:discNumber"]),
                "track_id": str(data["Metadata"]["mpris:trackid"]),
                "track_url": str(data["Metadata"]["xesam:url"]),
                "length": int(data["Metadata"]["mpris:length"]) / 1000000,
                "art_url": str(data["Metadata"]["mpris:artUrl"]),
            }
            self.recorder.record(new_track)

    @property
    def proxy(self):
        if self._proxy is None:
            self._proxy = self._session_bus.get_object(
                "org.mpris.MediaPlayer2.spotify", "/org/mpris/MediaPlayer2")
        return self._proxy

    @property
    def playback_status(self):
        """Retrieves Spotify's playback status from DBus."""
        spotify_properties = dbus.Interface(
            self.proxy, "org.freedesktop.DBus.Properties")
        return spotify_properties.Get(
            "org.mpris.MediaPlayer2.Player", "PlaybackStatus")

    def skip_next(self):
        """Skips to the next song"""
        iface = dbus.Interface(
            self.proxy, "org.mpris.MediaPlayer2.Player")
        return iface.Next()

    def stop(self):
        """Stop the player"""
        iface = dbus.Interface(
            self.proxy, "org.mpris.MediaPlayer2.Player")
        return iface.Pause()



def setup_sound():
    """
    Setup PulseAudio to redirect the Spotify stream to something we can record.
    """
    # Load null sink module if not already loaded
    output = sp.check_output(["pacmd", "list-sinks"])
    if "spotdump" not in output.decode("utf-8"):
        sp.check_output(["pactl", "load-module", "module-null-sink",
                         "sink_name=spotdump"])
        atexit.register(sp.check_output, ["pactl", "unload-module", "module-null-sink"])
    sp.check_output(["pactl", "set-sink-volume", "spotdump", "100%"])
    # Get index of running Spotify sink.
    sink_index = None
    output = sp.check_output(["pacmd", "list-sink-inputs"])
    sinks_by_name = {}
    _current_sink_index = None
    for line in output.decode("utf-8").split("\n"):
        line = line.strip()
        if line.startswith("index: "):
            current_sink_index = int(line.split(" ")[1])
        elif line.startswith("application.name = "):
            name = line[len("application.name = "):].strip('"')
            sinks_by_name[name] = current_sink_index
    # Move over if existing.
    if "Spotify" not in sinks_by_name:
        raise SpotifyNotFound
    sink_index = sinks_by_name["Spotify"]
    sp.check_output(["pactl", "set-sink-input-volume", str(sink_index), "100%"])
    log.debug("Spotify input found, moving to spotdump sink")
    sp.check_output(["pactl", "move-sink-input", str(sink_index), "spotdump"])


def parse_args():
    parser = argparse.ArgumentParser(
        description='Dump a Spotify playlist to MP3.')
    parser.add_argument('path',
        help='directory where the playlist should be dumped')
    parser.add_argument('--debug', action='store_true',
        help='output more information')
    args = parser.parse_args()
    if not os.path.isdir(args.path):
        parser.error("no such directory: {}".format(args.path))
    return args


def main():
    global MAINLOOP
    args = parse_args()
    if args.debug:
        LOG_CONFIG["level"] = logging.DEBUG
    logging.basicConfig(**LOG_CONFIG)
    try:
        setup_sound()
    except SpotifyNotFound:
        log.error("Error: Spotify not found! Make sure it is running.")
        sys.exit(1)
    recorder = Recorder(args.path, Playlist(), Encoder())
    bus = DBusManager(recorder)
    bus.setup()
    recorder.player = bus
    MAINLOOP = glib.MainLoop()
    try:
        MAINLOOP.run()
    except KeyboardInterrupt:
        log.info("Exiting on user request...")
        if recorder.is_playing:
            bus.stop()
            #recorder.stop(is_last=True)
            MAINLOOP.run() # One last time
        elif not recorder.encoder.is_idle:
            recorder.finish()
            MAINLOOP.run() # One last time


if __name__ == "__main__":
    main()
