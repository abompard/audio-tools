#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

u"""

Spotify dump
------------

Dumps a Spotify playlist in text format and CSV format.
Optionnaly associates a BPM text file formatted as such::

    BPM_intger [TAB] Artist - Title

And generates a line graph of the songs' BPMs across the playlist.

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""

from __future__ import print_function, unicode_literals

import sys
import csv
import os
import json
from argparse import ArgumentParser
from io import BytesIO

import requests
import pygal
import numpy
from bs4 import BeautifulSoup
from jinja2 import Template


SPOTIFY_URL = "http://open.spotify.com/user/{user}/playlist/{playlist}"
CACHE_PATH = os.path.expanduser("~/.cache/spotify-dump.json")
HTML_TEMPLATE = Template("""<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
  <title>Vitesse des morceaux - {{ basename }}</title>
  <script type="text/javascript" src="http://kozea.github.com/pygal.js/javascripts/svg.jquery.js"></script>
  <script type="text/javascript" src="http://kozea.github.com/pygal.js/javascripts/pygal-tooltips.js"></script>
  <style>
  body {
      background-color: black;
      color: #eee;
  }
  table#playlist {
      margin: 1em auto 0 auto;
      border-collapse: collapse;
  }
  table#playlist th {
      background-color: #333;
      padding: 0.5em;
  }
  table#playlist tr.even {
      background-color: #222;
  }
  table#playlist tr.active {
      background-color: #444;
  }
  table#playlist td {
      padding: 0.2em 0.5em;
  }
  </style>
  </head>
  <body>
    <figure>
      {{ svg }}
    </figure>
    <table id="playlist">
    <thead>
        <tr>
            <th>N°</th>
            <th>Artiste</th>
            <th>Titre</th>
            <th>Durée</th>
            <th>BPM</th>
        </tr>
    </thead>
    <tbody>
        {% for track in playlist %}
        <tr class="{{ loop.cycle('odd', 'even') }}">
            <td>{{ loop.index }}</td>
            <td>{{ track.artist|truncate(50)|escape }}</td>
            <td>{{ track.title|truncate(50)|escape }}</td>
            <td>{{ track.duration }}</td>
            <td>{% if track.bpm %}{{ track.bpm|int }}{% endif %}</td>
        </tr>
        {% endfor %}
    </tbody>
    </table>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script>
    $(function() {
        $("table#playlist td").hover(function() {
            var line = $(this).parent("tr");
            var index = line.prevAll("tr").length;
            var dot = $("svg circle.dot.reactive").eq(index);
            line.addClass("active");
            dot.css("stroke-width", "5px");
        }, function() {
            var line = $(this).parent("tr");
            var index = line.prevAll("tr").length;
            var dot = $("svg circle.dot.reactive").eq(index);
            line.removeClass("active");
            dot.css("stroke-width", "1px");
        });
    });
    </script>
  </body>
</html>
""")


class Playlist:
    def __init__(self, name):
        self.name = name
        self.tracks = []

    def append(self, track):
        self.tracks.append(track)

    def __iter__(self):
        return self.tracks.__iter__()

    def match_bpms(self, bpms):
        for track in self.tracks:
            key = "{0} - {1}".format(track.artist, track.title)
            try:
                track.bpm = bpms[key]
            except KeyError:
                print("Can't find the BPM for {}".format(key), file=sys.stderr)

    def set_track_cache(self, cache):
        Track.cache = cache
        #for track in self.tracks:
        #    track.cache = cache

    def check(self):
        trackids = [ t.trackid for t in self.tracks ]
        dupl_tids = [ tid for tid in trackids if trackids.count(tid) > 1 ]
        dupl_tracks = dict( (t.trackid, t) for t in self.tracks
                            if t.trackid in dupl_tids )
        for tid, track in dupl_tracks.iteritems():
            print("===> WARNING: duplicate track in positions {}: {} - {}".format(
                  " & ".join(str(i+1) for i, t in enumerate(trackids) if t == tid),
                  track.artist, track.title),
                  file=sys.stderr)

    def stats(self):
        stats = []
        stats.append("Tracks: {}".format(len(self.tracks)))
        bpms = numpy.array([ t.bpm for t in self.tracks if t.bpm is not None ])
        if bpms:
            #stats.append("Average BPM: {:.2}".format(sum(bpms) / len(bpms)))
            stats.append("Average BPM: {:.2f}".format(numpy.mean(bpms)))
            stats.append("Median BPM:  {:.2f}".format(numpy.median(bpms)))
            stats.append("Std dev BPM: {:.2f}".format(numpy.std(bpms)))
        return "\n".join(stats)

    def prettify(self):
        lines = ["=== {} ===".format(self.name)]
        for index, track in enumerate(self.tracks):
            line = []
            tracknumwidth = len(unicode(len(self.tracks)))
            line.append(unicode(index + 1).rjust(tracknumwidth, "0") + ".")
            line.append(track.prettify())
            lines.append(" ".join(line))
        return "\n".join(lines)

    def to_csv(self):
        result = BytesIO()
        csvfile = csv.writer(result)
        csvfile.writerow([ h.encode("utf-8") for h in Track.csv_header ])
        for track in self.tracks:
            row = []
            for field in track.to_csv():
                if field is None:
                    field = ""
                row.append(field.encode("utf-8"))
            csvfile.writerow(row)
        return result.getvalue()

    def to_svg(self):
        line_chart = pygal.Line(
                show_legend=False,
                title_font_size=9,
                label_font_size=5,
                major_label_font_size=6,
                legend_font_size=7,
                value_font_size=7,
                tooltip_font_size=7,
                height=200,
                )
        line_chart.title = "Vitesse des morceaux"
        line_chart.y_title = "BPM"
        line_chart.x_labels = [ unicode(n + 1) for n in range(len(self.tracks)) ]
        serie = []
        for track in self.tracks:
            t = { "label": "{} - {}".format(track.artist, track.title) }
            if track.bpm is None:
                t["value"] = None
            else:
                t["value"] = int(track.bpm)
            serie.append(t)
        line_chart.add('tracks', serie)
        return line_chart

    def to_graph(self, filename):
        line_chart = self.to_svg()
        line_chart.render_to_file(filename)

    def to_html(self, filename):
        line_chart = self.to_svg()
        line_chart.disable_xml_declaration = True
        line_chart.pretty_print = True
        output = HTML_TEMPLATE.render(
            basename=os.path.splitext(filename)[0],
            svg=line_chart.render(),
            playlist=self,
        )
        with open(filename, "w") as outfile:
            outfile.write(output.encode("utf-8"))



class Track:

    base_url = "http://ws.spotify.com/lookup/1/?uri=spotify:track:{0}"
    csv_header = ["Artiste", "Titre", "Durée", "BPM"]
    cache = None

    def __init__(self, trackid):
        self.trackid = trackid
        self.artist = None
        self.title = None
        self.duration = None
        self.bpm = None

    def load_metadata(self):
        if self.load_from_cache():
            return
        r = requests.get(self.base_url.format(self.trackid))
        soup = BeautifulSoup(r.text, "xml")
        tag = soup.find("name")
        if tag is not None:
            self.title = tag.string
        artists = []
        for artist in soup.find_all("artist"):
            artists.append(artist.find("name").string)
        self.artist = ", ".join(artists)
        tag = soup.find("length")
        if tag is not None:
            duration = float(tag.string)
            minutes = int(duration / 60)
            seconds = int(duration - 60 * minutes)
            self.duration = "{}:{:02}".format(minutes, seconds)
        self.save_to_cache()
        print("Loaded track {0}: {1}".format(self.trackid, self.prettify()))

    def load_from_cache(self):
        if self.trackid not in self.cache:
            return False
        for prop in ("artist", "title", "duration", "bpm"):
            setattr(self, prop, self.cache[self.trackid].get(prop))
        return True

    def save_to_cache(self):
        if self.trackid not in self.cache:
            self.cache[self.trackid] = {}
        for prop in ("artist", "title", "duration", "bpm"):
            self.cache[self.trackid][prop] = getattr(self, prop)

    def prettify(self):
        result = "{} - {} ({})".format(self.artist, self.title, self.duration)
        if self.bpm is not None:
            result += " ({:n} BPM)".format(self.bpm)
        return result

    def to_csv(self):
        row = [
            self.artist,
            self.title,
            self.duration or "",
        ]
        if self.bpm is not None:
            row.append("{:n}".format(self.bpm))
        return row

    def __eq__(self, other):
        if not isinstance(other, Track):
            raise ValueError("Can't compare a track with {}".format(other))
        return self.trackid == other.trackid

def get_playlist(source):
    if os.path.exists(source):
        playlist = get_playlist_from_file(source)
    elif source.startswith("http://"):
        playlist = get_playlist_from_url(source)
    elif source.startswith("spotify:user:"):
        url = SPOTIFY_URL.format(user=source.split(":")[2],
                                 playlist=source.split(":")[4])
        playlist = get_playlist_from_url(url)
    else:
        raise ValueError("Unknown playlist identifier")
    for track in playlist:
        track.load_metadata()
    return playlist

def get_playlist_from_url(pl_url):
    r = requests.get(pl_url)
    if r.status_code != 200:
        print("Can't get playlist {} from the web".format(pl_url),
              file=sys.stderr)
        return
    soup = BeautifulSoup(r.text)
    playlist = Playlist(soup.find("h1").string)
    soup_pl = soup.find("ul", id="tracks")
    for t in soup_pl.find_all("li"):
        duration = t.find("span", class_="duration").contents[1]
        playlist.append(Track(t["data-id"]))
    return playlist

def get_playlist_from_file(filepath):
    trackurls = unicode(open(filepath).read()).strip().split()
    name = os.path.splitext(os.path.basename(filepath))[0]
    playlist = Playlist(name)
    for trackurl in trackurls:
        trackid = trackurl.split("/")[-1]
        playlist.append(Track(trackid))
    return playlist


def load_bpms(filepath):
    result = {}
    with open(os.path.expanduser(filepath)) as bpm_file:
        csvfile = csv.reader(bpm_file, delimiter=b"\t")
        for row in csvfile:
            if len(row) < 2:
                continue
            result[row[1].decode("utf-8")] = float(row[0])
    return result


def main():
    parser = ArgumentParser(description="Dump Spotify playlists.")
    parser.add_argument("-b", "--bpmdb", help="text file containing "
                        "the song's measured BPMs")
    parser.add_argument("playlists", nargs="+",
                        help="text file with the songs URLs")
    args = parser.parse_args()
    if args.bpmdb:
        if not os.path.exists(args.bpmdb):
            parser.error("No such file: {}".format(args.bpmdb))
        bpms = load_bpms(args.bpmdb)
    else:
        bpms = None
    try:
        with open(CACHE_PATH) as track_cache_file:
            track_cache = json.load(track_cache_file)
    except (IOError, ValueError):
        track_cache = {}
    Track.cache = track_cache
    for pl_source in args.playlists:
        if pl_source.endswith(".txt"):
            parser.error("The playlist filename should not end in .txt, or it will be overwritten")
        playlist = get_playlist(pl_source)
        if bpms:
            playlist.match_bpms(bpms)
        with open("{}.txt".format(playlist.name), "w") as f:
            f.write(playlist.prettify().encode('utf-8'))
        with open("{}.csv".format(playlist.name), "w") as f:
            f.write(playlist.to_csv())
        if bpms:
            playlist.to_graph("{}.svg".format(playlist.name))
            playlist.to_html("{}.html".format(playlist.name))
        stats = playlist.stats()
        if stats:
            print(stats)
        playlist.check()
    with open(CACHE_PATH, "w") as track_cache_file:
        json.dump(track_cache, track_cache_file)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\rExiting on user request")
