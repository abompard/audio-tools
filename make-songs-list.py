#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
u"""

Make songs list
---------------

Create a PDF file with the list of Karaoke songs available to Performous.

Performous: http://performous.org/

.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later
"""

import os
import sys
import tempfile
import optparse
import atexit
import re
from collections import defaultdict
from pprint import pprint
from codecs import BOM_UTF8

import PIL

from reportlab.platypus import BaseDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, Frame, PageTemplate
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib import colors
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch


defaultPageSize = (defaultPageSize[1], defaultPageSize[0])
PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]
MARGIN = 8
COLSEP = 10
COLNUM = 4
FONTSIZE = 10

songs = []

def get_songs(cover_size, dpi, no_sort):
    if opts.input:
        available_songs = scan_file()
    else:
        available_songs = scan_folders()
    for index, available_song in enumerate(available_songs):
        song = process_file(available_song, cover_size, dpi)
        if song:
            songs.append(song)
        sys.stdout.write("\rAdding song %d/%d"
                         % (index+1, len(available_songs)))
        sys.stdout.flush()
    print
    print "Building PDF..."
    if not no_sort:
        songs.sort(cmp=sort_songs)

def sort_songs(x, y):
    try:
        key_x = x["artist"]
        key_y = y["artist"]
    except KeyError:
        print x, y
        raise
    for ignore in ["The ", "Les "]:
        if key_x.startswith(ignore):
            key_x = key_x[len(ignore):]
        if key_y.startswith(ignore):
            key_y = key_y[len(ignore):]
    if key_x == key_y:
        key_x = x["title"]
        key_y = y["title"]
    return cmp(key_x, key_y)

def scan_folders():
    songs_dirs = ["/usr/share/performous/songs",
                  "/usr/local/share/games/ultrastar/songs",
                  "/usr/local/share/games/fretsonfire/data/songs",
                  "/usr/share/games/ultrastar/songs",
                  "/usr/share/games/fretsonfire/data/songs",
                  "~/.ultrastar/songs",
                  "~/.fretsonfire/songs",
                 ]
    encoding = sys.getfilesystemencoding()
    available_songs = []
    for songs_dir in songs_dirs:
        songs_dir = os.path.expanduser(songs_dir)
        if not os.path.exists(songs_dir):
            continue
        for root, dirs, files in os.walk(songs_dir):
            for cur_file in files:
                cur_file = cur_file.decode(encoding)
                if not cur_file.lower().endswith(".txt"):
                    continue
                available_songs.append(os.path.join(root, cur_file))
    return available_songs

def scan_file():
    available_songs = []
    songs_file = open(opts.input, "r")
    for line in songs_file:
        song_file = line.strip().decode("utf-8")
        if not song_file.lower().endswith(".txt"):
            continue
        if not os.path.exists(song_file):
            print >>sys.stderr, "WARNING: file %s in the input file " \
                               % song_file + "cannot be found !"
            continue
        available_songs.append(song_file)
    songs_file.close()
    return available_songs

def process_file(filepath, cover_size, dpi):
    song = {"path": filepath}
    file_h = open(filepath, "r")
    for line in file_h:
        if line.startswith(BOM_UTF8):
            print "BOM detected in", filepath
            line = line[len(BOM_UTF8):]
        for tag in ["artist", "title", "cover", "background", "video"]:
            if line.startswith("#%s:" % tag.upper()):
                song[tag] = ":".join(line.split(":")[1:]).strip()
                try:
                    song[tag] = unicode(song[tag], "utf8")
                except UnicodeDecodeError:
                    song[tag] = unicode(song[tag], "iso-8859-1")
    file_h.close()
    if "title" not in song:
        print filepath, "has no title"
        song["title"] = ""
    if song["title"].endswith(" (*)"):
        song["title"] = song["title"][:-4]
        song["flawed"] = True
    filter_re = re.compile("(.*)\s+[\(\[].*[\)\]]")
    filter_match = filter_re.match(song["title"])
    if filter_match is not None:
        song["shorttitle"] = filter_match.group(1)
    else:
        song["shorttitle"] = song["title"]
    for tag in ["cover", "background"]:
        if tag not in song:
            continue
        try:
            song["image"] = os.path.realpath(os.path.join(
                            os.path.dirname(filepath), song[tag]))
        except UnicodeDecodeError:
            print "This path contains a non-ascii character:", filepath
            raise
        if not cover_size or not os.path.exists(song["image"]):
            del song["image"]
            continue
        # Resize the image
        tmpimg, tmpimg_path = tempfile.mkstemp(
                                prefix="mksonglist-", suffix=".jpg")
        img = PIL.Image.open(song["image"])
        img.thumbnail((cover_size * dpi, cover_size * dpi), PIL.Image.ANTIALIAS)
        img.save(os.fdopen(tmpimg, "w"), "JPEG")
        atexit.register(os.remove, tmpimg_path)
        song["image"] = tmpimg_path
        break
    if "video" in song:
        videofile = os.path.join(os.path.dirname(filepath), song["video"])
        if not os.path.exists(videofile):
            del song["video"]
    return song

def filter_songs(karaoke):
    new_songs = []
    for song in songs:
        title = song["title"]
        if not karaoke and title.count("(Karaoke)"):
            continue
        similar = [ s for s in new_songs
                    if s["artist"] == song["artist"]
                        and s["shorttitle"] == song["shorttitle"] ]
        if similar:
            print "Renaming %s -> %s" % (similar[0]["title"], similar[0]["shorttitle"])
            similar[0]["title"] = similar[0]["shorttitle"]
            continue
        new_songs.append(song)
    songs[:] = new_songs

def make_pdf(output, title, cover_size, no_sort):
    doc = BaseDocTemplate(
                output, title=title,
                pagesize=defaultPageSize,
          )
    col_width = (PAGE_WIDTH - 2 * MARGIN - (COLNUM - 1) * COLSEP) / COLNUM
    col_height = PAGE_HEIGHT - 2 * MARGIN
    if cover_size:
        vspace_left = col_height % (cover_size + 2) # used to bottom-align the 2nd and 3rd columns
        vspace_left_left = (col_height - 2*cover_size) % (cover_size + 2) # used to bottom-align the left column
    else:
        vspace_left = 0
        vspace_left_left = 30
    frames = []
    for col_id in range(COLNUM):
        margin_left = MARGIN + (col_width + COLSEP) * col_id
        if col_id == 0:
            top_padding = vspace_left_left + 2*cover_size
        else:
            top_padding = vspace_left
        frame = Frame(margin_left, MARGIN, col_width, col_height,
                      leftPadding=0, rightPadding=0, topPadding=top_padding,
                      bottomPadding=0, id="col%d" % col_id, showBoundary=0)
        frames.append(frame)
    frame_right = Frame(PAGE_WIDTH - MARGIN - col_width, MARGIN,
                        col_width, col_height, leftPadding=0,
                        rightPadding=0, topPadding=vspace_left,
                        bottomPadding=0, id="right")
    doc.addPageTemplates(PageTemplate("threecols", frames=frames))
    Story = []
    text_style = ParagraphStyle(name="labels", fontSize=FONTSIZE)
    text_with_artist_style = ParagraphStyle(name="labels", fontSize=FONTSIZE, leftIndent=10)
    artist_header_style = ParagraphStyle(name="artist-header", fontSize=FONTSIZE, spaceBefore=15)
    table_style = TableStyle([
        ("VALIGN", (0,0), (-1,-1), "MIDDLE"),
        #("LEADING", (0,0), (-1,-1), 1),
        ("TOPPADDING", (0,0), (-1,-1), 1),
        ("BOTTOMPADDING", (0,0), (-1,-1), 2),
        ("LEFTPADDING", (0,0), (0,-1), 0),
        ("RIGHTPADDING", (0,0), (-1,-1), 0),
        ("LEFTPADDING", (1,0), (1,-1), 5),
        #("BOX", (0,0), (-1,-1), 0.25, colors.gray),
    ])
    table_contents = []
    artists_count = defaultdict(lambda: 0)
    for song in songs:
        artists_count[song["artist"]] += 1
    artists_headers = [ a for a in artists_count if artists_count[a] >= 3 ]
    if no_sort:
        artists_headers = []
    in_artist_header = None
    for song in songs:
        try:
            if song["artist"] in artists_headers:
                if in_artist_header != song["artist"]:
                    artist_header = Paragraph(
                            "<b>%s</b>" % song["artist"].replace("&", "&amp;"),
                            artist_header_style)
                    if "image" in song:
                        image = Image(song["image"], cover_size, cover_size)
                    else:
                        image = u"\u2022"
                    table_contents.append([image, artist_header])
                in_artist_header = song["artist"]
            else:
                in_artist_header = None
            image = None
            if in_artist_header:
                text = song["title"]
                if "image" in song:
                    del song["image"]
            else:
                text = "<b>%s</b> - %s" % (song["artist"], song["title"])
            text = text.replace("&", "&amp;")
            if "flawed" in song or not "video" in song:
                text = "<i>%s</i>" % text
            if in_artist_header:
                label = Paragraph(text, text_with_artist_style)
            else:
                label = Paragraph(text, text_style)
            if "image" in song:
                image = Image(song["image"], cover_size, cover_size)
                #image.drawHeight = cover_size *I.drawHeight / I.drawWidth
                #image.drawWidth = cover_size
            elif not in_artist_header:
                image = u"\u2022"
            else:
                #image = Spacer(1, cover_size)
                image = None
            row = [image, label]
            table_contents.append(row)
        except Exception:
            print >>sys.stderr, "Error when printing %s" % repr(song)
            raise
    Story.append(Table(table_contents,
                       colWidths=(cover_size + 2, col_width - (cover_size + 2)),
                       style=table_style))
    doc.build(Story)

def parse_opts():
    #usage = "usage: %prog [options]"
    parser = optparse.OptionParser()
    parser.add_option("-o", "--output", dest="output", default="songs.pdf",
                      help="Output file name (default: %default)")
    parser.add_option("-t", "--title", dest="title", default="Songs",
                      help="PDF title (default: %default)")
    parser.add_option("-k", "--karaoke", dest="karaoke", action="store_true",
                      help="List the 'karaoke' versions (default: False)")
    parser.add_option("-c", "--cover-size", dest="cover_size", default=32,
                      type="int", help="Cover size (default: %default pt)")
    parser.add_option("--dpi", dest="dpi", default=96,
                  help="Image resolution in Dots Per Inch (default: %default)")
    parser.add_option("-i", "--input", dest="input", metavar="FILE",
                      help="Read the song list from this file instead of "
                           "scanning the folders")
    parser.add_option("--no-sort", action="store_true",
                      help="Do not sort the songs by artist and title")
    opts, args = parser.parse_args()
    if len(args) != 0:
        parser.error("No arguments allowed")
    if opts.input and not os.path.exists(opts.input):
        parser.error("The file %s does not exist" % opts.input)
    return opts

if __name__ == '__main__':
    opts = parse_opts()
    get_songs(opts.cover_size, opts.dpi, opts.no_sort)
    filter_songs(opts.karaoke)
    make_pdf(opts.output, opts.title, opts.cover_size, opts.no_sort)
    print "PDF written to", opts.output
