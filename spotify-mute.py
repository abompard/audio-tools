#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

u"""

Spotify-Mute
------------

Uses PulseAudio to mute Spotify when an advertisement is being played.

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""

import sys
import time
import csv
import dbus
from subprocess import Popen, PIPE, STDOUT, call


SLEEP_TIME_DEFAULT = 1
SLEEP_TIME_NOT_PLAYING = 5


class ControllerError(Exception): pass

class Controller(object):
    """Abstract controller class: mutes and unmutes Spotify"""

    name = None

    def connect(self, bus):
        """Initialization, like connecting to DBus"""
        raise NotImplementedError

    def mute(self):
        """Mute Spotify"""
        raise NotImplementedError

    def unmute(self):
        """Unmute Spotify"""
        raise NotImplementedError



class PaCtl(Controller):
    """Use the pactl command (it must be in your path)"""

    name = "pactl"

    def __init__(self):
        self._input_id = None

    def connect(self, bus):
        """Just check that the binary is available"""
        proc = Popen(["which", "pactl"], stdout=PIPE, stderr=STDOUT)
        proc.communicate()
        if proc.returncode != 0:
            raise ControllerError("The 'pactl' binary is not available")

    def _find_spotify_id(self):
        """ Find the Spotify-specific mixer.
        Only available after Spotify has started playing, so don't do this at
        connect()-time.
        """
        proc = Popen(["pactl", "list", "short", "clients"],
                     stdout=PIPE)
        client_id = None
        output = proc.communicate()[0].strip().split("\n")
        for client in csv.reader(output, delimiter="\t"):
            if client[-1] == "spotify":
                client_id = client[0]
                break
        if client_id is None:
            raise ControllerError("Spotify not found in the PulseAudio clients")
        proc = Popen(["pactl", "list", "short", "sink-inputs"],
                     stdout=PIPE)
        output = proc.communicate()[0].strip().split("\n")
        for s_input in csv.reader(output, delimiter="\t"):
            if s_input[2] == client_id:
                self._input_id = s_input[0]
        if self._input_id == None:
            raise ControllerError("Spotify not found in the PulseAudio sink inputs")

    def mute(self):
        if self._input_id is None:
            self._find_spotify_id()
        call(["pactl", "set-sink-input-mute", self._input_id, "1"])

    def unmute(self):
        if self._input_id is None:
            self._find_spotify_id()
        call(["pactl", "set-sink-input-mute", self._input_id, "0"])



class KMix(Controller):
    """Use KMix over DBus"""

    name = "kmix"

    def __init__(self):
        self.props = None
        self._bus = None
        self._mixers = None

    def connect(self, bus):
        """Search for the Spotify mixer and store it"""
        self._bus = bus
        try:
            proxy = self._bus.get_object('org.kde.kmix', '/Mixers')
            self._mixers = proxy.Get('org.kde.KMix.MixSet', 'mixers',
                               dbus_interface='org.freedesktop.DBus.Properties')
        except dbus.exceptions.DBusException, e:
            raise ControllerError(e)

    def _find_spotify_mixer(self):
        """ Find the Spotify-specific mixer.
        Only available after Spotify has started playing, so don't do this at
        connect()-time.
        """
        for mixer in self._mixers:
            proxy = self._bus.get_object('org.kde.kmix', mixer)
            controls = proxy.Get(
                    'org.kde.KMix.Mixer', 'controls',
                    dbus_interface='org.freedesktop.DBus.Properties')
            for control in controls:
                proxy_c = self._bus.get_object('org.kde.kmix', control)
                name = proxy_c.Get(
                        'org.kde.KMix.Control', 'readableName',
                        dbus_interface='org.freedesktop.DBus.Properties')
                if name == "Spotify: Spotify":
                    self.props = dbus.Interface(
                            proxy_c,
                            dbus_interface='org.freedesktop.DBus.Properties')
                    return
        raise ControllerError("Spotify mixer not found")

    def _set_mute_status(self, status):
        if self.props is None:
            self._find_spotify_mixer()
        try:
            self.props.Set('org.kde.KMix.Control', 'mute', status)
        except dbus.exceptions.DBusException:
            # org.freedesktop.DBus.Error.UnknownObject: DBus name changed?
            self.props = None
            self._set_mute_status(status)

    def mute(self):
        self._set_mute_status(True)

    def unmute(self):
        self._set_mute_status(False)



class Spotify(object):
    """Connect to Spotify over DBus and request song info"""

    def __init__(self):
        self.props = None

    def connect(self, bus):
        proxy = bus.get_object(
                'org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2')
        self.props = dbus.Interface(
                proxy, dbus_interface='org.freedesktop.DBus.Properties')

    def metadata(self):
        return self.props.Get('org.mpris.MediaPlayer2.Player', 'Metadata')



def check_ad(spotify, controller):
    """ Get the current track, and mute or unmute if it's an ad or not """
    global status
    try:
        metadata = spotify.metadata()
    except dbus.exceptions.DBusException, e:
        print "Could not get the current track, maybe you closed spotify."
        sys.exit()
    if not metadata:
        if status["sleep_time"] == SLEEP_TIME_DEFAULT:
            print "Could not get the current track, maybe not playing yet."
        status["sleep_time"] = SLEEP_TIME_NOT_PLAYING
        return
    trackid = metadata["mpris:trackid"]
    if trackid == status["trackid"]:
        return # no track change
    status["artist"] = metadata["xesam:artist"][0]
    status["title"] = metadata["xesam:title"]
    status["trackid"] = metadata["mpris:trackid"]
    duration = metadata["mpris:length"] / 1000000
    duration_readable = "%d:%02d" % (duration / 60, duration % 60)
    if is_an_ad(metadata) and not status["playing_ad"]:
        print "Playing an ad (%s), muting" % duration_readable
        status["playing_ad"] = True
        controller.mute()
        #print dict((str(k), unicode(v)) for k, v in metadata.iteritems())
    elif not is_an_ad(metadata) and status["playing_ad"]:
        print "Ad ended, unmuting"
        controller.unmute()
        status["playing_ad"] = False
    if not status["playing_ad"]:
        print "Playing %s - %s (%s)" % (
              status["artist"], status["title"], duration_readable)
    status["sleep_time"] = SLEEP_TIME_DEFAULT



def is_an_ad(metadata):
    """ Test if the supplied track is likely to be an ad """
    artist = metadata["xesam:artist"][0]
    title = metadata["xesam:title"]
    duration = metadata["mpris:length"] / 1000000
    return (str(metadata["xesam:contentCreated"]) == ""
            or duration < 60 # ads are rarely above 30s, this is a good margin
            or artist == title == "Spotify")


def get_controller(session_bus):
    """Get the most appropriate volume controller class"""
    controller = None
    for controller_class in [PaCtl, KMix]:
        controller = controller_class()
        try:
            controller.connect(session_bus)
        except ControllerError:
            print ("Can't initialize the %s volume controller"
                   % controller_class.name)
            continue
        else:
            break
    return controller


def main():
    global status

    session_bus = dbus.SessionBus()
    controller = get_controller(session_bus)
    if controller is None:
        print >>sys.stderr, ("Can't find a working volume controller, "
                             "please install 'pactl' (from pulseaudio-utils)")
        sys.exit(1)
    print "Using %s to control the volume" % controller.name

    spotify = Spotify()
    try:
        spotify.connect(session_bus)
    except dbus.exceptions.DBusException, e:
        print "Can't connect to Spotify, aborting."
        sys.exit(1)

    status = {
        "artist": None,
        "title": None,
        "trackid": None,
        "playing_ad": False,
        "sleep_time": SLEEP_TIME_DEFAULT
    }

    # Unmute in case Spotify was stopped while playing an ad (mute status is
    # remebered by PulseAudio across restarts)
    try:
        controller.unmute()
    except ControllerError, e:
        pass # if the mixer is not found then it's not muted, ignore

    # Main loop, poll every second (by default)
    while True:
        try:
            check_ad(spotify, controller)
            #print "Sleeping %ss" % status["sleep_time"]
            time.sleep(status["sleep_time"])
        except ControllerError, e:
            print >>sys.stderr, e
            break



if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print "\rExiting on user request"
