#!/bin/sh

playlist=$1

if [ ! -e "$playlist" ]; then
    echo "File not found: $playlist"
    exit 1
fi

grep -v -e '^$' -e '^#' "$playlist" | while read filename; do
    id3info "$(dirname "$playlist")/$filename" \
        | grep "^=== COMM" | head -1 \
        | sed -r -e 's/.* (spotify:track:[a-zA-Z0-9]+)$/\1/'
done
